from django.db import models

# Create your models here.

class Pessoa(models.Model):
    nome = models.CharField('Nome', max_length=100)
    email = models.EmailField('Email')

    def __str__(self):
        return self.nome