from django.shortcuts import render, redirect

from django.views.generic import View, TemplateView, CreateView, ListView

from .forms import PessoaForm
from .models import Pessoa

# Create your views here.

# def home(request):
#     if request.method == 'POST':
#         form = PessoaForm(request.POST)
#         if form.is_valid():
#             form.save()
#     form = PessoaForm()
#     return render(request, 'home.html', {'form': form})

def lista_pessoas(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'lista_pessoas.html', {'pessoas': pessoas})

class TemplateHomeView(TemplateView):
    template_name = 'home.html'

class PessoaCreateView(CreateView):
    template_name = 'home.html'
    model = Pessoa
    form_class = PessoaForm
    success_url = '/lista-pessoas/'

class PessoaListView(ListView):
    template_name = 'lista_pessoas.html'
    model = Pessoa

class HomeTemplateView(View):
    template_name = 'home.html'
    form_class = PessoaForm

    def get(self, request, *args, **kwargs):
        form = self.form_class
        return render(request, self.template_name, {'form': form})
    
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return redirect('lista_pessoas')
        else:
            return render(request, self.template_name, {'form': form})